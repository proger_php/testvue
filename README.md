## Тестовое задание FRONT

Фронт – Vuesj2 + Vuex.  
Бэк - сделать mock с возможностью быстрого переключения на рабочий бэк (api) 
### Задача: Система подсчета стоимости доставки: 
1) На интерфейсе необходима возможность добавления/редактирование/удаления посылок 
2) Онлайн расчет стоимости. 
а) Внутренняя доставка посылок - 3 руб за 1 км 
б) Международная доставка - 5 руб за 1 км 
#### Требования: 
1) Авторизация jwt (эмуляция работы с токенами + разделение доступа по ролям) 
2) Верстка bootstrap v4 или на выбор


# vue-mock-data

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
