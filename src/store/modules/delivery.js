import client from 'api-client'

export default {
    namespaced: true,
    state: {
        posts: ''
    },
    mutations: {
        setPosts(state, posts) {
            state.posts = posts
        }
    },
    actions: {
        fetchPosts({commit}) {
            return client
                .fetchPosts()
                .then(posts => {
                    commit('setPosts', posts)
                })
        },
        deletePost({commit}, posts) {
            return client
                .deletePost(posts)
                .then(posts => {
                    commit('setPosts', posts)
                })
        },
        updatePost({commit}, posts) {
            return client
                .updatePost(posts)
                .then(posts => {
                    commit('setPosts', posts)
                })
        },
        addPost({commit}, posts) {
            return client
                .addPost(posts)
                .then(posts => {
                    commit('setPosts', posts)
                })
        }
    },
    getters: {
        lists: state => {
            return state.posts;
        },
    }
}