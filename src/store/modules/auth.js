import client from 'api-client'

export default {
    namespaced: true,
    state: {
        status: '',
        token: localStorage.getItem('token') || '',
        user: localStorage.getItem('authUser') || ""
    },
    mutations: {
        auth_request(state) {
            state.status = 'loading'
        },
        auth_success(state, token) {
            state.status = 'success'
            state.token = token
            state.user = localStorage.getItem('authUser')
        },
        auth_error(state) {
            state.status = 'error'
        },
        logout(state) {
            state.status = ''
            state.token = ''
            state.user = ""
        },
    },
    actions: {
        login({commit}, user) {
            return new Promise((resolve, reject) => {
                commit('auth_request')
                client.login(user).then(resp => {
                    const token = resp.data.token
                    const authUser = resp.data.user

                    localStorage.setItem('token', token)
                    localStorage.setItem('authUser', JSON.stringify(authUser))
                    commit('auth_success', token)
                    resolve(token)
                }).catch(err => {
                    commit('auth_error')
                    localStorage.removeItem('token')
                    localStorage.removeItem('authUser')
                    reject(err)
                });
            })
        },
        logout({commit}) {
            return new Promise((resolve) => {
                localStorage.removeItem('token')
                localStorage.removeItem('authUser')
                commit('logout')
                resolve()
            })
        }
    },
    getters: {
        isLoggedIn: state => !!state.token,
        authStatus: state => state.status,
        authUser: state => state.user,
    }
}