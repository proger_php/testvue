import Vuex from 'vuex';
import Vue from 'vue';
import delivery from './modules/delivery';
import auth from './modules/auth';

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        delivery,
        auth,
    }
});
