import users from './data/users'
import posts from './data/posts'
import * as env from './../../const'
import jwt from 'jsonwebtoken'

const fetch = (mockData, time = 0) => {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(mockData)
        }, time)
    })
}

export default {
    fetchPosts() {
        // At the first boot, save to the local repository
        if(!localStorage.getItem('delivery')){
            localStorage.setItem('delivery', JSON.stringify(posts))
        }
        let delivery = localStorage.getItem('delivery');
        return fetch(delivery, 1000) // wait 1s before returning posts
    },
    deletePost(posts) {
        localStorage.setItem('delivery', JSON.stringify(posts))

        return fetch(JSON.stringify(posts), 1000) // wait 1s before returning posts
    },
    updatePost(posts) {
        localStorage.setItem('delivery', JSON.stringify(posts))

        return fetch(JSON.stringify(posts), 1000) // wait 1s before returning posts
    },
    addPost(posts) {
        localStorage.setItem('delivery', JSON.stringify(posts))

        return fetch(JSON.stringify(posts), 1000) // wait 1s before returning posts
    },
    login(user) {
        return new Promise((resolve,reject) => {
            let authUser = users.filter(item => {
                return user.email.toLowerCase() === item.email.toLowerCase() && user.password === item.password;
            });

            if (authUser.length) {
                let user = authUser.shift();
                let token = jwt.sign(
                    user,
                    env.JWT_SECRET,
                    {
                        expiresIn: 86400 // expires in 24 hours
                    });

                resolve({
                    data: {user,token}
                })
            }

            reject('error');
        })
    }
}