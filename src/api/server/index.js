import * as env from './../../const'
import axios from "axios";

export default {
    fetchPosts () {
        return axios
            .get(`${(env.VUE_APP_API_URL)}/posts`)
            .then(response => response.data)
    },
    // eslint-disable-next-line no-unused-vars
    deletePost(posts) {
        // TODO Need an implementation
    },
    // eslint-disable-next-line no-unused-vars
    updatePost(posts) {
        // TODO Need an implementation
    },
    // eslint-disable-next-line no-unused-vars
    addPost(posts) {
        // TODO Need an implementation
    },
    login(user) {
        return axios
            .post(`${(env.VUE_APP_API_URL)}/auth/login`,user)
            .then(response => response.data)
    }
}