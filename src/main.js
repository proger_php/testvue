import Vue from 'vue'
import Axios from 'axios'
import App from './App.vue'
import VueRouter from 'vue-router'
import router from './router'
import Gate from './util/roles'
import store from './store/index'
import * as env from './const'

Vue.use(VueRouter);
Vue.use(store);

Vue.router = router;
Vue.prototype.$http = Axios;
Vue.prototype.$env = {...env};
window.$gate = new Gate();


const token = localStorage.getItem('token')
if (token) {
    Vue.prototype.$http.defaults.headers.common['Authorization'] = token
}

Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: h => h(App),
}).$mount('#app')
