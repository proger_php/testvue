import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from './components/Home.vue'
import Login from './components/Login.vue'
import Secure from './components/Secure.vue'


// Routes
const routes = [
    {
        path: '/',
        name: 'home',
        component: Home,
    },
    {
        path: '/login',
        name: 'login',
        component: Login
    },
    {
        path: '/secure',
        name: 'secure',
        component: Secure,
        meta: {
            requiresAuth: true
        }
    },
];


const router = new VueRouter({
    history: true,
    mode: 'history',
    linkExactActiveClass: 'active',
    routes,
});

router.beforeEach((to, from, next) => {
    // Check whether logged in or not
    if (to.matched.some(record => record.meta.requiresAuth)) {
        const authUser = JSON.parse(window.localStorage.getItem('authUser'));

        if (!authUser || !Vue.prototype.$http.defaults.headers.common['Authorization']) {
            next({name: 'login'})
            return
        }
    }

    // We’ll check which path has come. If Reset, then we will reset storage
    if ((to.name === "reset")) {
        window.localStorage.clear();
    }

    next()
})

export default router
