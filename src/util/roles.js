export default class Gate {

    constructor() {
        this.ADMIN = 'Admin';
        this.SUADM = 'SuperAdmin';
        this.USER = 'User';
    }

    isAdmin(user) {
        if (user) {
            return JSON.parse(user).role === this.ADMIN
        }
        return false;
    }

    isSadm(user) {
        if (user) {
            return JSON.parse(user).role === this.SUADM
        }
        return false;
    }

    isUser(user) {
        if (user) {
            return JSON.parse(user).role === this.USER
        }
        return false;
    }

}
